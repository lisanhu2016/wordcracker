use wordcracker::*;
use std::collections::HashSet;

fn main() {
    let cfg = parse();
    std::process::exit(run(&cfg));
}

fn run(cfg: &Config) -> i32 {
    let dict = get_dictionary();
    match process(cfg, dict) {
        Ok(words) => {
            for word in words {
                println!("{}", word);
            }
            return 0;
        },
        Err(msg) => {
            eprintln!("{}", msg);
            return 1;
        },
    }
}

pub fn get_dictionary() -> HashSet<String> {
    use std::fs::File;    
    use std::io::{BufReader, BufRead};

    let mut dict = HashSet::new();
    let reader = BufReader::new(File::open("words.txt").unwrap());
    let lines = reader.lines();

    for line in lines {
        dict.insert(line.unwrap().to_lowercase());
    }
    dict
}

fn parse() -> Config {
    use clap::*;

    let matches = App::new("Word Cracker").about(r#"This tool is used to crack words for game wordscapes.
User can enter the characters to be used in the command line.
If you want to specify the lengths of the words you can do that using -l.
The program will exhaust all combinations possible with the given length and lookup the data in /usr/share/dict/words and output those words that are in the dictionary.
    "#)
    .arg(Arg::with_name("chars").help("characters to use").multiple(true).required(true))
    .arg(Arg::with_name("length").help("length of the word").short("l").takes_value(true)).get_matches();

    let characters: Vec<&str> = matches.values_of("chars").unwrap().collect();
    let characters: Vec<char> = characters
        .iter()
        .map(|s| s.chars())
        .flatten()
        .collect();
    let mut length = characters.len();
    if let Some(len) = matches.value_of("length") {
        length = str::parse(len).unwrap();
    }

    Config { characters, length }
}