use std::collections::HashSet;

pub fn process(cfg: &Config, dict: HashSet<String>) -> Result<Vec<String>, String> {
    use itertools::Itertools;

    let length = cfg.characters.len();
    if cfg.length > length {
        return Err(format!(
            "Wrong length entered. |abc| = {}, with length = {}",
            length, cfg.length
        ));
    }
    let perms = (0..length).permutations(cfg.length);
    let mut words = HashSet::new();

    for perm in perms {
        let s: Vec<char> = perm.iter().map(|&i| cfg.characters[i]).collect();
        let s: String = s.into_iter().collect();
        if dict.contains(&s) {
            words.insert(s);
        }
    }

    let words = words.into_iter().collect();
    Ok(words)
}

pub struct Config {
    pub characters: Vec<char>,
    pub length: usize,
}

/// Expose the JNI interface for android below
#[cfg(target_os = "android")]
#[allow(non_snake_case)]
pub mod android {
    extern crate jni;

    use self::jni::objects::{JClass, JString};
    use self::jni::sys::{jcharArray, jobjectArray, jsize, jstring};
    use self::jni::JNIEnv;
    use super::*;
    use std::ffi::CStr;

    fn into_dictionary(words: String) -> HashSet<String> {
        let mut dict = HashSet::new();
        for line in words.lines() {
            dict.insert(line.to_lowercase());
        }
        dict
    }

    #[no_mangle]
    pub unsafe extern "C" fn Java_com_example_wordcracker_WordCracker_execute(
        env: JNIEnv,
        _: JClass,
        length: jsize,
        j_chars: jcharArray,
        j_words: JString,
    ) -> jobjectArray {
        let words = CStr::from_ptr(env.get_string(j_words).unwrap().as_ptr()).to_str().unwrap().to_owned();
        let dict = into_dictionary(words);

        let size = env.get_array_length(j_chars).unwrap();
        let mut buf = vec![0; size as usize];
        env.get_char_array_region(j_chars, 0, &mut buf).unwrap();
        let characters: Vec<char> = buf
            .iter()
            .map(|jc| std::char::from_u32(*jc as u32).unwrap())
            .collect();
        let length = length as usize;
        let cfg = Config { characters, length };
        let jstring_class = env.find_class("java/lang/String").unwrap();

        match process(&cfg, dict) {
            Ok(words) => {
                let len = words.len();
                let arr = env
                    .new_object_array(len as jsize, jstring_class, *env.new_string("").unwrap())
                    .unwrap();
                for i in 0..len {
                    env.set_object_array_element(
                        arr,
                        i as jsize,
                        *env.new_string(&words[i]).unwrap(),
                    )
                    .unwrap();
                }
                return arr;
            }
            Err(_) => {
                let arr = env
                    .new_object_array(0, jstring_class, *env.new_string("").unwrap())
                    .unwrap();
                return arr;
            }
        }
    }
}
