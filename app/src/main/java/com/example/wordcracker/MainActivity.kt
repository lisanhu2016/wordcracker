package com.example.wordcracker

import android.content.res.AssetManager
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.util.regex.Pattern

class MainActivity : AppCompatActivity(), View.OnClickListener {

    init {
        System.loadLibrary("wordcracker")
    }

    private lateinit var dict: String

    fun read_all(reader: BufferedReader): String {
        val sbuf = StringBuffer()
        for (line: String in reader.lines()) {
            sbuf.append(line)
            sbuf.append('\n')
        }
        return sbuf.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val reader = BufferedReader(InputStreamReader(assets.open("words.txt")))
        dict = read_all(reader)

        val help = findViewById<Button>(R.id.help)
        help.setOnClickListener(this)

        val clear = findViewById<Button>(R.id.clear)
        clear.setOnClickListener(MainClearButtonAdapter(this))
    }

    override fun onClick(v: View?) {
        class MyAdapter(private val myDataset: Array<String>) :
            RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

            inner class MyViewHolder(val myView: View) : RecyclerView.ViewHolder(myView) {
                val tView = myView.findViewById<TextView>(R.id.textView)
            }

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): MyAdapter.MyViewHolder {
                // create a new view
                val myView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.word_list_layout, parent, false)
                return MyViewHolder(myView)
            }

            override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
                holder.tView.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources.getDimension(R.dimen.textsize)
                )
                holder.tView.text = myDataset[position]
            }

            override fun getItemCount() = myDataset.size
        }

        val ca = findViewById<TextInputEditText>(R.id.characters).text.toString().toCharArray()
        val len = try {
            Integer.parseInt(findViewById<TextInputEditText>(R.id.length).text.toString())
        } catch (_: Exception) {
            -1
        }

        val wc = WordCracker();
        var words = wc.execute(if (len <= ca.size && len >= 0) len else ca.size, ca, dict)

        val regex = findViewById<TextInputEditText>(R.id.regex).text.toString()
        if (!regex.isEmpty()) {
            val pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
            val words_new = mutableListOf<String>()
            for (word: String in words) {
                if (pattern.matcher(word).find()) {
                    words_new.add(word)
                }
            }
            words = words_new.toTypedArray()
        }

        val adapter = MyAdapter(words)
        val rcview = findViewById<RecyclerView>(R.id.words_list)
        rcview.adapter = adapter
        rcview.layoutManager = LinearLayoutManager(this)
    }
}