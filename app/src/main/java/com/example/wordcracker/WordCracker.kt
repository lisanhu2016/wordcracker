package com.example.wordcracker

import android.content.res.AssetManager

class WordCracker {
    external fun execute(length: Int, j_chars: CharArray, words: String): Array<String>
}