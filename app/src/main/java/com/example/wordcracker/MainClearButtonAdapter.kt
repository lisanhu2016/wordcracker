package com.example.wordcracker

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.google.android.material.textfield.TextInputEditText


class MainClearButtonAdapter(var main: MainActivity) : View.OnClickListener {
    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun setFocus(view: TextInputEditText, activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        view.requestFocus()
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    override fun onClick(v: View?) {
        val chars = main.findViewById<TextInputEditText>(R.id.characters)
        val length = main.findViewById<TextInputEditText>(R.id.length)
        val regex = main.findViewById<TextInputEditText>(R.id.regex)

        chars.text?.clear()
        length.text?.clear()
        regex.text?.clear()
//        hideKeyboard(main)
        setFocus(chars, main)
    }
}